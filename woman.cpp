#include <iostream>
#include <stdexcept>
#include <variant>
#include <vector>
#include <array>
#include <sstream>
#include <iomanip>
#include <optional>
#include <algorithm>
#include <cmath>

class Hole {};

class Wall {
    int height_;
  public:
    Wall(int height): height_(height) {}
    int height() { return height_; }
    void SetHeight(int height) { height_ = height; }
};

class Cell {
    std::variant<Hole, Wall> cell_;
  public:
    Cell(): cell_(Hole()) {}
    Cell(Hole h): cell_(h) {}
    Cell(Wall w): cell_(w) {}

    template<typename R, typename Hf, typename Wf>
        R Visit(Hf OnHole, Wf OnWall) const;

    template<typename R, typename Hf, typename Wf>
        R Visit(Hf OnHole, Wf OnWall);
};

template<typename R, typename Hf, typename Wf>
R Cell::Visit(Hf OnHole, Wf OnWall) const {
    switch(cell_.index()) {
        case 0:
            return OnHole(std::get<0>(cell_));
        case 1:
            return OnWall(std::get<1>(cell_));
        default:
            throw std::logic_error("Unknown variant in Cell::Visit()");
    }
};

template<typename R, typename Hf, typename Wf>
R Cell::Visit(Hf OnHole, Wf OnWall) {
    switch(cell_.index()) {
        case 0:
            return OnHole(std::get<0>(cell_));
        case 1:
            return OnWall(std::get<1>(cell_));
        default:
            throw std::logic_error("Unknown variant in Cell::Visit()");
    }
}

class Vector {
    int x_;
    int y_;
  public:
    Vector() {}
    Vector(int x, int y): x_(x), y_(y) {}

    int& x() { return x_; }
    int& y() { return y_; }
    int x() const { return x_; }
    int y() const { return y_; }

    Vector operator+(const Vector& other) const {
        return Vector(x_ + other.x(), y_ + other.y());
    }
    Vector operator-(const Vector& other) const {
        return Vector(x_ - other.x(), y_ - other.y());
    }
    Vector& operator += (const Vector& other) {
        x_ += other.x();
        y_ += other.y();
        return *this;
    }
    Vector& operator -= (const Vector& other) {
        x_ -= other.x();
        y_ -= other.y();
        return *this;
    }
    Vector operator-() const {
        return Vector(-x_, -y_);
    }

    bool operator==(const Vector& other) const {
        return (x_ == other.x()) && (y_ == other.y());
    }
    bool operator!=(const Vector& other) const {
        return !((*this) == other);
    }
};

class Position {
    int x_;
    int y_;
  public:
    Position() {}
    Position(int x, int y): x_(x), y_(y) {}

    int& x() { return x_; }
    int& y() { return y_; }
    int x() const { return x_; }
    int y() const { return y_; }

    Position operator+(const Vector& other) const {
        return Position(x_ + other.x(), y_ + other.y());
    }
    Position operator-(const Vector& other) const {
        return Position(x_ - other.x(), y_ - other.y());
    }
    Vector operator-(const Position& other) const {
        return Vector(x_ - other.x(), y_ - other.y());
    }
    Position& operator += (const Vector& other) {
        x_ += other.x();
        y_ += other.y();
        return *this;
    }
    Position& operator -= (const Vector& other) {
        x_ -= other.x();
        y_ -= other.y();
        return *this;
    }

    bool operator==(const Position& other) const {
        return (x_ == other.x()) && (y_ == other.y());
    }
    bool operator!=(const Position& other) const {
        return !((*this) == other);
    }
};

class Board {
    int board_size_;
    std::vector<Cell> cells_;
  public:
    Board(int board_size): board_size_(board_size),
                           cells_(board_size * board_size) {}

    int size() const {
        return board_size_;
    }

    bool IsValid(Position pos) const {
        return (pos.x() >= 0) && (pos.y() >= 0) && (pos.x() < board_size_) && (pos.y() < board_size_);
    }

    Cell& operator[] (Position pos) {
        if (!IsValid(pos)) {
            throw std::out_of_range("Board position out of range");
        }
        return cells_[pos.x() + pos.y() * board_size_];
    }

    Cell operator[] (Position pos) const {
        if (!IsValid(pos)) {
            throw std::out_of_range("Board position out of range");
        }
        return cells_[pos.x() + pos.y() * board_size_];
    }
};

const int NumPlayers = 2;
enum class PlayerID { Me, Opponent };
const std::array<PlayerID, NumPlayers> player_ids = { PlayerID::Me, PlayerID::Opponent };

PlayerID OtherPlayer(PlayerID player) {
    switch (player) {
        case PlayerID::Me:
            return PlayerID::Opponent;
        case PlayerID::Opponent:
            return PlayerID::Me;
        default:
            throw std::logic_error("Unknown PlayerID in OtherPlayer()");
    }
}

class Game {
    std::array<std::vector<Position>, NumPlayers> units_;
    Board board_;
  public:
    Game(int board_size, int units_per_player):
        units_({ std::vector<Position>(units_per_player),
                 std::vector<Position>(units_per_player) }),
        board_(board_size) {}

    int ImpenetrableWallHeight() const {
        return 4;
    }

    Board& board() {
        return board_;
    }

    const Board& board() const {
        return board_;
    }

    int UnitsPerPlayer() const {
        return units_[0].size();
    }

    std::vector<Position>& PlayerUnits(PlayerID player) {
        return units_[static_cast<int>(player)];
    }

    const std::vector<Position>& PlayerUnits(PlayerID player) const {
        return units_[static_cast<int>(player)];
    }

    bool IsFinished() const;
};

Game read_initial_game_state(std::istream& input) {
    int board_size;
    int units_per_player;
    input >> board_size; input.ignore();
    input >> units_per_player; input.ignore();
    return Game(board_size, units_per_player);
}

Cell cell_from_char(char c) {
    if (c == '.') {
        return Cell(Hole());
    } else if ( c >= '0' && c <= '9' ) {
        return Cell(Wall(c - '0'));
    } else {
        std::ostringstream msg;
        msg << "Invalid symbol "
            << "'" << c << "' (" << std::hex << int(c) << ")"
            << " in game board description";
        throw std::invalid_argument(msg.str());
    }
};

char cell_to_char(Cell c) {
    return c.Visit<char>(
        [](Hole h) { return '.'; },
        [](Wall w) { return static_cast<char>('0' + w.height()); }
    );
}

void read_game_state(std::istream& input, Game& game) {
    const int board_size = game.board().size();
    for (int y = 0; y < board_size; ++y) {
        for (int x = 0; x < board_size; ++x) {
            char c;
            input >> c;
            try {
                game.board()[Position(x, y)] = cell_from_char(c);
            } catch (std::invalid_argument& exc) {
                std::ostringstream msg;
                msg << exc.what() << " at (" << x << ", " << y << ")";
                throw std::invalid_argument(msg.str());
            }
        }
        input.ignore();
    }

    for (int unit = 0; unit < game.UnitsPerPlayer(); ++unit ) {
        int x;
        int y;
        input >> x >> y; input.ignore();
        game.PlayerUnits(PlayerID::Me)[unit] = Position(x, y);
    }
    for (int unit = 0; unit < game.UnitsPerPlayer(); ++unit ) {
        int x;
        int y;
        input >> x >> y; input.ignore();
        game.PlayerUnits(PlayerID::Opponent)[unit] = Position(x, y);
    }

    {
        int legal_actions;
        input >> legal_actions; input.ignore();
        for (int i = 0; i < legal_actions; ++i) {
            input.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
}

struct WorldDirection {
    const char* name;
    Vector delta;
};

const std::array<WorldDirection, 8> directions = {
    WorldDirection {"E", Vector(1, 0)},
    {"NE", Vector(1, -1)},
    {"N", Vector(0, -1)},
    {"NW", Vector(-1, -1)},
    {"W", Vector(-1, 0)},
    {"SW", Vector(-1, 1)},
    {"S", Vector(0, 1)},
    {"SE", Vector(1, 1)}
};

// TODO: These several functions should be part of a Player class or something.
bool is_valid_step(const Game& game, PlayerID player, int unit, Vector direction) {
    const Position position = game.PlayerUnits(player)[unit];
    const Board& board = game.board();
    if (!board.IsValid(position)) {
        throw std::invalid_argument("is_valid_step: Original position is outside the board boundaries");
    }
    const Position next = position + direction;
    if (!board.IsValid(next)) {
        return false;
    }

    // No other units there
    for (PlayerID test_player : player_ids) {
        const std::vector<Position>& units = game.PlayerUnits(test_player);
        for (int i = 0; i < units.size(); ++i) {
            if (test_player != player || i != unit) {
                if (units[i] == next) {
                    return false;
                }
            }
        }
    }

    // Acceptable change in height
    const int origin_height = board[position].Visit<int>(
        [](Hole h) -> int { throw std::invalid_argument("is_valid_step: Original position is a hole"); },
        [](Wall w) { return w.height(); }
    );
    const int max_height = game.ImpenetrableWallHeight();
    return board[next].Visit<bool>(
        [](Hole h) -> bool { return false; },
        [origin_height, max_height](Wall w) -> bool {
            return (w.height() < max_height) && (w.height() <= origin_height + 1);
        }
    );
}

bool is_valid_build_target(const Game& game, PlayerID player, int unit, Vector step_direction, Vector direction) {
    const Position original_position = game.PlayerUnits(player)[unit];
    const Position position = original_position + step_direction;
    const Board& board = game.board();
    const Position build_position = position + direction;
    if (!board.IsValid(build_position)) {
        return false;
    }

    // No other units there
    for (PlayerID test_player : player_ids) {
        const std::vector<Position>& units = game.PlayerUnits(test_player);
        for (int i = 0; i < units.size(); ++i) {
            if (test_player != player || i != unit) {
                if (units[i] == build_position) {
                    return false;
                }
            }
        }
    }

    // Acceptable height
    const int max_height = game.ImpenetrableWallHeight();
    return board[build_position].Visit<bool>(
        [](Hole h) -> bool { return false; },
        [max_height](Wall w) -> bool {
            return w.height() < max_height;
        }
    );
}

// bool is_world_direction(Vector direction) {
//     if ((std::abs(direction.x()) > 1) || (std::abs(direction.y()) > 1)) {
//         return false;
//     }
//     if ((direction.x() == 0) && (direction.y() == 0)) {
//         return false;
//     }
//     return true;
// }

bool is_valid_push_and_build(const Game& game, PlayerID player, int unit, Vector target_direction, Vector push_direction) {
//     if (!is_world_direction(target_direction) || !is_world_direction(push_direction)) {
//         return false;
//     }
    // Push direction must be "close" to target direction.
    {
        const Vector delta = push_direction - target_direction;
        if (std::abs(delta.x()) + std::abs(delta.y()) > 1) {
            return false;
        }
    }
    const Position player_pos = game.PlayerUnits(player)[unit];
    const Position target_pos = player_pos + target_direction;
    if (!game.board().IsValid(target_pos)) {
        return false;
    }
    // An opponent's unit must be in the target cell.
    {
        const std::vector<Position>& other_units = game.PlayerUnits(OtherPlayer(player));
        const std::vector<Position>::const_iterator it = std::find(other_units.begin(), other_units.end(), target_pos);
        if (it == other_units.end()) {
            return false;
        }
        const int other_unit = it - other_units.begin();
        // The push must be an otherwise-valid move.
        return is_valid_step(game, OtherPlayer(player), other_unit, push_direction);
    }
}

class AbstractMove {
  protected:
    Game start_state_;
    PlayerID player_;
  public:
    AbstractMove(const Game& start_state, PlayerID player):
        start_state_(start_state), player_(player) {}

    const Game& StartState() const {
        return start_state_;
    }

    PlayerID Player() const {
        return player_;
    }

    virtual Game EndState() const = 0;
    virtual bool IsScoringMove() const = 0;
    virtual std::ostream& Declare(std::ostream&) const = 0;
};

class StayStuck: public AbstractMove {
  public:
    StayStuck(const Game& start_state, PlayerID player):
        AbstractMove(start_state, player) {}

    Game EndState() const override {
        return StartState();
    }

    bool IsScoringMove() const override {
        return false;
    }

    std::ostream& Declare(std::ostream& stream) const override {
        return stream << "NONE";
    }
};

class MoveAndBuild: public AbstractMove {
    int unit_;
    WorldDirection move_direction_;
    WorldDirection build_direction_;
  public:
    MoveAndBuild(const Game& start_state, PlayerID player, int unit,
                 const WorldDirection& move_direction,
                 const WorldDirection& build_direction):
        AbstractMove(start_state, player), unit_(unit),
        move_direction_(move_direction), build_direction_(build_direction) {
        if ( !is_valid_step(start_state_, player_, unit_, move_direction_.delta) ||
             !is_valid_build_target(start_state_, player_, unit_,
                                    move_direction_.delta, build_direction_.delta)) {
            throw std::invalid_argument("MoveAndBuild: the move is invalid.");
        }
    }

    int Unit() const {
        return unit_;
    }

    const WorldDirection& MoveDirection() const {
        return move_direction_;
    }

    const WorldDirection& BuildDirection() const {
        return build_direction_;
    }

    Game EndState() const override {
        Game res = start_state_;
        res.PlayerUnits(player_)[unit_] += move_direction_.delta;
        const Position build_position = res.PlayerUnits(player_)[unit_] + build_direction_.delta;
        res.board()[build_position] = res.board()[build_position].Visit<Cell>(
            [](Hole) -> Cell { throw std::logic_error("MoveAndBuild::EndState(): Build position is a hole."); },
            [](Wall w) { return Wall(w.height() + 1); }
        );
        return res;
    }

    bool IsScoringMove() const override {
        const Position dest = start_state_.PlayerUnits(player_)[unit_] + move_direction_.delta;
        const int scoring_height = start_state_.ImpenetrableWallHeight() - 1;
        return start_state_.board()[dest].Visit<bool>(
            [](Hole) -> bool { throw std::logic_error("MoveAndBuild::IsScoringMove: target cell is a hole."); },
            [scoring_height](Wall w) { return w.height() >= scoring_height; }
        );
    }

    std::ostream& Declare(std::ostream& stream) const override {
        return stream << "MOVE&BUILD " << Unit() << " "
                  << MoveDirection().name << " "
                  << BuildDirection().name << " ";
    }
};

class PushAndBuild: public AbstractMove {
    int unit_;
    WorldDirection target_direction_;
    WorldDirection push_direction_;
  public:
    PushAndBuild(const Game& start_state, PlayerID player, int unit,
                 const WorldDirection& target_direction,
                 const WorldDirection& push_direction):
        AbstractMove(start_state, player), unit_(unit),
        target_direction_(target_direction), push_direction_(push_direction) {
        if ( !is_valid_push_and_build(start_state_, player_, unit_,
                                      target_direction_.delta, push_direction_.delta)) {
            throw std::invalid_argument("PushAndBuild: the move is invalid.");
        }
    }

    int Unit() const {
        return unit_;
    }

    const WorldDirection& TargetDirection() const {
        return target_direction_;
    }

    const WorldDirection& PushDirection() const {
        return push_direction_;
    }

    Game EndState() const override {
        Game res = start_state_;
        const Position target_position = res.PlayerUnits(player_)[unit_] + target_direction_.delta;
        // Push
        std::vector<Position>& other_units = res.PlayerUnits(OtherPlayer(player_));
        std::vector<Position>::iterator it = std::find(other_units.begin(), other_units.end(), target_position);
        if (it == other_units.end()) {
            throw std::logic_error("PushAndBuild::EndState(): Target position does not contain opponent's unit.");
        }
        *it += push_direction_.delta;
        // Build
        res.board()[target_position] = res.board()[target_position].Visit<Cell>(
            [](Hole) -> Cell { throw std::logic_error("PushAndBuild::EndState(): Build position is a hole."); },
            [](Wall w) { return Wall(w.height() + 1); }
        );
        return res;
    }

    bool IsScoringMove() const override {
        return false;
    }

    std::ostream& Declare(std::ostream& stream) const override {
        return stream << "PUSH&BUILD " << Unit() << " "
                  << TargetDirection().name << " "
                  << PushDirection().name << " ";
    }
};

class AnyMove: public AbstractMove {
    std::variant<StayStuck, MoveAndBuild, PushAndBuild> the_move_;
  public:
    AnyMove(const StayStuck& move):
        AbstractMove(move.StartState(), move.Player()), the_move_(move) {}
    AnyMove(const MoveAndBuild& move):
        AbstractMove(move.StartState(), move.Player()), the_move_(move) {}
    AnyMove(const PushAndBuild& move):
        AbstractMove(move.StartState(), move.Player()), the_move_(move) {}
    AnyMove(StayStuck&& move):
        AbstractMove(move.StartState(), move.Player()), the_move_(move) {}
    AnyMove(MoveAndBuild&& move):
        AbstractMove(move.StartState(), move.Player()), the_move_(move) {}
    AnyMove(PushAndBuild&& move):
        AbstractMove(move.StartState(), move.Player()), the_move_(move) {}

    Game EndState() const override {
        return std::visit([](const AbstractMove& move){ return move.EndState(); }, the_move_);
    }

    bool IsScoringMove() const override {
        return std::visit([](const AbstractMove& move){ return move.IsScoringMove(); }, the_move_);
    }

    std::ostream& Declare(std::ostream& stream) const override {
        return std::visit([&stream](const AbstractMove& move) -> std::ostream& { return move.Declare(stream); }, the_move_);
    }

    bool IsStuck() const {
        return the_move_.index() == 0;
    }
};

int moves_considered;

std::vector<AnyMove> ListValidMoves(const Game& game, PlayerID player) {
    std::vector<AnyMove> res;
    int units_per_player = game.PlayerUnits(player).size();
    for (int unit = 0; unit < units_per_player; ++unit) {
        if (game.board().IsValid(game.PlayerUnits(player)[unit])) {
            for (const WorldDirection& move_direction : directions) {
                if (is_valid_step(game, player, unit, move_direction.delta)) {
                    for (const WorldDirection& build_direction : directions) {
                        if (is_valid_build_target(game, player, unit,
                                                  move_direction.delta, build_direction.delta)) {
                            res.emplace_back(MoveAndBuild(game, player, unit, move_direction, build_direction));
                            ++moves_considered;
                        }
                    }
                }
            }
        }
    }
    for (int unit = 0; unit < units_per_player; ++unit) {
        if (game.board().IsValid(game.PlayerUnits(player)[unit])) {
            for (int dir_index = 0; dir_index < directions.size(); ++dir_index) {
                const WorldDirection& target_direction =
                    directions[(dir_index + 1) % directions.size()];
                for (int push_dir_index = dir_index; push_dir_index < dir_index + 3; ++push_dir_index) {
                    const WorldDirection& push_direction =
                        directions[push_dir_index % directions.size()];
                    if (is_valid_push_and_build(game, player, unit,
                                                target_direction.delta, push_direction.delta)) {
                        res.emplace_back(PushAndBuild(game, player, unit, target_direction, push_direction));
                        ++moves_considered;
                    }
                }
            }
        }
    }
    if (res.empty()) {
        ++moves_considered;
        return {StayStuck(game, player)};
    }
    return res;
}

bool Game::IsFinished() const {
    for (PlayerID player : player_ids) {
        for (const AnyMove& move : ListValidMoves(*this, player)) {
            if (!move.IsStuck()) {
                return false;
            }
        }
    }
    return true;
}

// TODO: Note: With current implementation, a DecisionNode is owned by its parent.
// With such ownership structure, we cannot effectively reuse the relevant parts
// of the decision tree between moves.
class DecisionEdge;

class DecisionNode {
    // Heuristic algorithm's params.
    static constexpr double heuristic_default_uncertainty_ = 10;
    static constexpr double heuristic_freedom_bonus_amount_ = 0.01;
    static constexpr double heuristic_freedom_bonus_decay_ = 10;
    static constexpr double heuristic_height_bonus_ = 0.2;
    static constexpr double heuristic_initiative_bonus_ = 1.0;
    // State
    Game game_;
    PlayerID player_;
    std::array<int, NumPlayers> scores_;
    double heuristic_score_;
    double heuristic_uncertainty_;
    //std::optional<std::vector<std::tuple<MoveAndBuild, DecisionNode>>> children_;
    std::optional<std::vector<DecisionEdge>> children_;
  public:
    DecisionNode(const Game& game, PlayerID player, const std::array<int, NumPlayers> scores):
        game_(game), player_(player), scores_(scores) {
        heuristic_score_ = heuristic_initiative_bonus_;
        for (const Position& pos : game.PlayerUnits(player)) {
            if (game.board().IsValid(pos)) {
                heuristic_score_ += heuristic_height_bonus_ * game.board()[pos].Visit<double>(
                    [](Hole) -> double { throw std::logic_error("DecisionNode: player position in a hole"); },
                    [](Wall w) -> double { return w.height(); }
                );
            }
        }
        heuristic_uncertainty_ = heuristic_default_uncertainty_;
    }

    // Accessors.
    const Game& GameState() const {
        return game_;
    }

    PlayerID Player() const {
        return player_;
    }

    const std::array<int, NumPlayers> Scores() const {
        return scores_;
    }

    double HeuristicScore() const {
        return heuristic_score_;
    }

    double HeuristicUncertainty() const {
        return heuristic_uncertainty_;
    }

  private:
    void UpdateHeuristic();

  public:
    std::vector<DecisionEdge>& Children() {
        if (!children_.has_value()) {
            std::vector<AnyMove> moves = ListValidMoves(game_, player_);
            std::vector<DecisionEdge>& res = children_.emplace();
            res.reserve(moves.size());
            for (const AnyMove& move : moves) {
                std::array<int, NumPlayers> new_scores = scores_;
                if (move.IsScoringMove()) {
                    new_scores[static_cast<int>(player_)] += 1;
                }
                res.emplace_back(this, move, DecisionNode(
                    move.EndState(),
                    OtherPlayer(player_),
                    new_scores
                ));
            }
            UpdateHeuristic();
        }
        return children_.value();
    }

    // expand one unexpanded descendant.
    void Expand();

    std::optional<DecisionEdge*> BestMove();
};

class DecisionEdge {
    DecisionNode* parent_;
    AnyMove move_;
    DecisionNode child_;

  public:
    DecisionEdge(DecisionNode* parent, const AnyMove& move, DecisionNode child):
        parent_(parent), move_(move), child_(child) {}

    DecisionNode& Parent() {
        return *parent_;
    }

    const DecisionNode& Parent() const {
        return *parent_;
    }

    const AnyMove& Move() const {
        return move_;
    }

    DecisionNode& Child() {
        return child_;
    }

    const DecisionNode& Child() const {
        return child_;
    }

    // Heuristic score of the child from the point of view of the parent.
    const double EffectiveHeuristicScore() const {
        double effective_score = Child().HeuristicScore();
        if (Child().Player() != Parent().Player()) {
            effective_score = -effective_score;
        }
        const PlayerID player = Parent().Player();
        const PlayerID opponent = OtherPlayer(player);
        effective_score += Child().Scores()[static_cast<int>(player)] -
                           Child().Scores()[static_cast<int>(opponent)] -
                           Parent().Scores()[static_cast<int>(player)] +
                           Parent().Scores()[static_cast<int>(opponent)];
        return effective_score;
    }
};

void DecisionNode::UpdateHeuristic() {
    if (!children_.has_value()) {
        return;
    }
    const std::vector<DecisionEdge>& children = children_.value();
    if (children.empty()) {
        // We have no moves.
        if (game_.IsFinished()) {
            heuristic_score_ = 0.0;
            heuristic_uncertainty_ = 0.0;
        } else {
            heuristic_uncertainty_ = heuristic_default_uncertainty_;
            heuristic_score_ = -heuristic_uncertainty_;
        }
        return;
    }
    std::optional<double> max_score;
    for (const DecisionEdge& edge : children) {
        const DecisionNode& child = edge.Child();
        const double effective_score = edge.EffectiveHeuristicScore();
        if (!max_score.has_value() || effective_score > max_score.value()) {
            max_score = effective_score;
            heuristic_uncertainty_ = child.HeuristicUncertainty();
        }
    }
    heuristic_score_ = max_score.value() - heuristic_freedom_bonus_amount_;
    for (const DecisionEdge& edge : children) {
        const DecisionNode& child = edge.Child();
        const double effective_score = edge.EffectiveHeuristicScore();
        heuristic_score_ += heuristic_freedom_bonus_amount_ * std::exp(
            heuristic_freedom_bonus_decay_ *
            (effective_score - max_score.value()) / child.HeuristicUncertainty());
    }
}

void DecisionNode::Expand() {
    if (!children_.has_value()) {
        // We are the "unexpanded descendant".
        Children();
        return;
    }
    std::vector<DecisionEdge>& children = children_.value();
    if (children.empty()) {
        // Nothing to expand.
        return;
    }
    DecisionNode* to_expand = nullptr;
    double best_score;
    for (DecisionEdge& edge : children) {
        DecisionNode& child = edge.Child();
        const double effective_score = edge.EffectiveHeuristicScore();
        if ((to_expand == nullptr) ||
            (best_score < effective_score + child.HeuristicUncertainty())) {
            to_expand = &child;
            best_score = effective_score + child.HeuristicUncertainty();
        }
    }
    to_expand -> Expand();
    UpdateHeuristic();
}

std::optional<DecisionEdge*> DecisionNode::BestMove() {
    std::vector<DecisionEdge>& children = Children();
    std::optional<DecisionEdge*> res;
    for (DecisionEdge& edge : children) {
        if (!res.has_value() ||
            res.value()->EffectiveHeuristicScore() <
                edge.EffectiveHeuristicScore()) {
            res = &edge;
        }
    }
    return res;
}

const int decision_tree_depth = 200;
const int max_moves_considered_per_turn = 1500;

int main(int argc, char* argv[]) {
    Game game = read_initial_game_state(std::cin);

    while (true) {
        read_game_state(std::cin, game);

        moves_considered = 0;
        DecisionNode decision_tree(game, PlayerID::Me, {0, 0});
        for (int i = 0; i < decision_tree_depth &&
                        moves_considered < max_moves_considered_per_turn; ++i) {
            decision_tree.Expand();
        }
        const std::optional<DecisionEdge*> move = decision_tree.BestMove();
        if (!move.has_value()) {
            std::cout << "NONE" << std::endl;
            throw std::logic_error("No valid moves found");
        } else {
            move.value()->Move().Declare(std::cout) <<
                "h = " << decision_tree.HeuristicScore() <<
                " m = " << moves_considered <<
                std::endl;
        }
    }
}
